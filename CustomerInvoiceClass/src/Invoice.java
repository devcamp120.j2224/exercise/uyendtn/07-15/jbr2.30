public class Invoice {
    int id;
    Customer customer;
    double amount;

    public int getId() {
        return id;
    }
    
    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public Invoice() {
    }

    public Invoice(int id, Customer customer, double amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }

    public int getCustomerId() {
        return customer.getId();
    }
    public String getCustomerName() {
        return customer.getName();
    }
    public int getCustomerDiscount() {
        return customer.getDiscount();
    }
    
    public double getAmountAfterDiscount() { 
        double amountAfterDiscount = amount  - amount*(this.getCustomerDiscount())/100; 
        return amountAfterDiscount;
    }

    public String toString() {
        System.out.println(this.getCustomerDiscount());
        return "Invoice[id= " + id + ", " + customer.toString() + ", amount = " + this.getAmountAfterDiscount() + "]";
    }
}
