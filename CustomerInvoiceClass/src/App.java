public class App {
    public static void main(String[] args) throws Exception {
        //4. tạo 2 customer và in ra systemout
        Customer customer1 = new Customer(1, "OHMM", 10);
        Customer customer2 = new Customer(2, "XTRA", 20);

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());

        //5.tạo 2 invoice và in ra system out
        Invoice invoice1 = new Invoice(1, customer1, 1000);
        Invoice invoice2 = new Invoice(2, customer2, 2000);

        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());
    }
}
